healthResponseStatus="down"
expectedResponse="up"
iterationCount=0
while [ "$healthResponseStatus" != "$expectedResponse" ] 
	do
		echo "Attempt # $((iterationCount+1))"
		if [ $iterationCount == 17 ]
		then
			echo "Time limit reached, exiting..."
			sleep 2
			exit 0
		fi  
		if [ $iterationCount != 0 ]
		then
			echo "Sleeping for 20 seconds and then trying again"
			sleep 20
		fi 
		iterationCount=$((iterationCount+1))
		healthResponse=$(curl -s --location --request GET 'http://127.0.0.1:8080/health')
		healthResponseStatus=$(echo $healthResponse | sed -n 's|.*"status":"\([^"]*\)".*|\1|p')
	done
echo "Status is up and running!"
sleep 2;