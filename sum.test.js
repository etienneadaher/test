const request = require('request');

function doRequest(url) {
  return new Promise(function (resolve, reject) {
    request(url, function (error, res, body) {
      if (!error && res.statusCode == 200) {
        resolve(body);
      } else {
        reject(error);
      }
    });
  });
}

test('Call localhost 8080', async () => {
  const result = await doRequest('http://127.0.0.1:8080/');
  console.log(`rezuz::: [${result}]`)
  expect(result).toBe('Hello MOTOOOzzzz 555 :::: [8080]');
});