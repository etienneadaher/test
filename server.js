const express = require('express')
const app = express()
const { PORT = 8080 } = process.env

const multer = require('multer')
const upload = multer({ dest: 'uploads/' });

app.get('/', (req, res) => {
  res.send(`Hello MOTOOOzzzz 555 :::: [${PORT}]`)
})
app.get('/health', (req, res) => {
  res.send({status: 200, data: {status: 'up'}})
});

app.listen(PORT, () => {
  console.log(`Example app listening at http://localhost:${PORT}`)
})

app.post('/upload', upload.single('file'), function (req, res, next) {
  const fileName = req.file ? req.file.originalname : 0;
  res.send(`UPLOADed FILE :::: [${fileName}]`)
})
