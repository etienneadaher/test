# Abstract Playwright Testing

Our project uses Playwright to automate end to end tests on Abstract User. These tests are designed to use our staging platform as the testing target. More information on Playwright here: https://playwright.dev/docs/intro

## Setup

The easiest way to setup playwright is to install with npm then use npx to download the browser data. 

npm i -D @playwright/test
npx playwright install

Also install node packages into this project by running

npm i

in this directory.

## Running Tests Locally

In order to run tests on your own machine, either see package.json for some 'npm run' shortcuts, or use:

FIREFOX=1 playwright test

The FIREFOX=1 part is to set the environment variable so we know what browser to use. Browsers can't be added normally to the projects section in playwright.config.ts because this means they run in parallel. Our tests, specficially the actions that are performed on the target website, do not play well when the browsers are running in parallel. This can be changed in the future to make sure certain data are randomized enough (ie username when creating a user).

In order to debug the tests as they run, set the environment variable PWDEBUG=1. To stop debugging, unset this variable.

export PWDEBUG=1
unset PWDEBUG

## Running Tests Through Bitbucket Pipeline

The tests are 'officially' run through bitbucket's pipeline. Under the AbstractUser project, there is a separate playwright pipeline. For now, this has to be run manually - in the future it will run on a schedule, something like once a day in the middle of the night.